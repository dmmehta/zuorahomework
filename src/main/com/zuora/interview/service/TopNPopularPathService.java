package com.zuora.interview.service;

public interface TopNPopularPathService {
	
	void setup(String[][] data);
	
	String[] getTopNPopularPaths(int n); 

}
