package com.zuora.interview.service;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.zuora.interview.dto.UserNavigationDTO;

public class TopNPopularPathServiceImpl implements TopNPopularPathService, PropertyChangeListener {
	
	private Map<String, Integer> popularPaths;
	
	public TopNPopularPathServiceImpl() {
		popularPaths = new HashMap<>();
	}

	@Override
	public void setup(String[][] data) {
		
		Map<String, UserNavigationDTO> userNavigations = new HashMap<>();
		
		for(int i=0; i< data.length; i++) {
			String user = data[i][0];
			UserNavigationDTO userNavigation = null;
			if(!userNavigations.containsKey(user)) {
				userNavigation = new UserNavigationDTO(user);
				userNavigation.addPropertyChangeListener(this); 
				userNavigations.put(user, userNavigation);
			} else {
				userNavigation = userNavigations.get(user);
			}
			userNavigation.addNavigation(data[i][1]);
		}
		
	}

	@Override
	public String[] getTopNPopularPaths(int n) {
		return popularPaths.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(n).map(e -> e.getKey() + ":" + e.getValue()).toArray(String[]::new);
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		List<String> path = (java.util.List) evt.getNewValue();
		String navigation = path.stream().collect(Collectors.joining("->"));
		
		Integer count = 0;
		
		if(popularPaths.containsKey(navigation)) {
			count = popularPaths.get(navigation);
		}

		popularPaths.put(navigation, ++count);
	}

}
