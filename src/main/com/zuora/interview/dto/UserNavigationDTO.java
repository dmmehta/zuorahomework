package com.zuora.interview.dto;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;

public class UserNavigationDTO {
	
	private static final int NAVIGATION_DEPTH = 3;
	
	private PropertyChangeSupport support;
	
	private String user;
	
	private LinkedList<String> navigation;

	public UserNavigationDTO(String user) {
		super();
		support = new PropertyChangeSupport(this);
		this.user = user;
		this.navigation = new LinkedList<>();
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }
 
    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        support.removePropertyChangeListener(pcl);
    }
	
	
	public void addNavigation(String path) {
		navigation.addLast(path);
		
		if(navigation.size() > NAVIGATION_DEPTH ) {
			navigation.removeFirst();
		} 
		
		if(navigation.size() == NAVIGATION_DEPTH) {
			support.firePropertyChange("navigation", null, navigation);
		}
	}


}